# Resonator
This is a small 2D-puzzle game I built using Unity and Wwise for both Windows and Mac. Resonator refers to a robot who can control a sonic orb, and this sonic orb can resonate with sonic elements in order to break them into collectable sound souls, and then use collected sonic souls to solve puzzles. The purpose of this project is to explore the functionalities provided by Wwise and how I can use those to create immersive gameplay experience for players.
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/JNdwvcKNHUI/0.jpg)](https://www.youtube.com/watch?v=JNdwvcKNHUI)

## [Version](/CHANGELOG.md)

## Installation

1. Install [Unity](https://unity3d.com/).


2. Install [Wwise](https://www.audiokinetic.com).


3. **Clone** the project to your machine.


4. Integrating Wwise into Unity project.

	- Open Wwise Launcher, and left click **UNITY** at the top

	- **Browse** for your project

	- Hit **integrate Wwise into Project...** button on your selected project

	- Check both **Mac & Windows** in Deployment Platform

	- Hit **integrate** button at the bottom and wait for the process to finish
	
	![](/resources/wwise_unity_integration.gif)


5. Generate soundbanks for the project.

	- Using Wwise Launcher to launch this project's Wwise project at ```/Resonator_WwiseProject/Resonator_WwiseProject.wproj```

	- At the top bar, select **Layouts** -> **SoundBanks**

	- Check platforms you want to support

	- Right click on **Main** soundbank -> **Generate Soundbank(s) for all platforms**

	![](/resources/generating_soundbanks.gif)

## Build
	
Follow [this guide](https://www.audiokinetic.com/library/edge/?source=Unity&id=pg__deploy.html) to build the project, if you wish to build the project on other platforms besides Windows and Mac, make sure both Unity and Wwise can support those platforms.

## 3rd Party Assets

- [THE ROBOT - FREE SPRITE](https://www.gameart2d.com/)

- [WITCH & MAGE - GAME SPRITES](https://www.gameart2d.com/character-spritesheet-15.html)

- [LAVA CAVE PLATFORMER GAME TILESET](https://www.gameart2d.com/lava-cave-platformer-tileset.html)

- [Zippy Water 2D](https://www.assetstore.unity3d.com/en/#!/content/59741)

- [Ancient Game Sound Effects Collection](http://epicstockmedia.com/product/ancient-game)

## [License](/LICENSE.md)