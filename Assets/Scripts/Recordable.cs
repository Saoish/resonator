﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Recordable : Elemental {
    SpriteRenderer HighLightMask;
    public abstract Property Property{ get; }    
    public static float timeRequired = 1f;

    Color Transparent = new Color(0f, 0f, 0f, 0f);
    Color HightLightGreen = new Color(0f, 1f, 0.09f, 0.392f);
    Color HighLightRed = new Color(1f, 0f, 0f, 0.392f);

    public static Dictionary<Property, string> PropertySFXs = new Dictionary<Property, string>() {
        //{Property.Wind,Collection.Wind },
        {Property.Water,Collection.Stream },
        {Property.Ice,Collection.Blizzard },
        {Property.Fire,Collection.Flame },
        {Property.Electric,Collection.ElectricLoop }
    };

    protected SpriteRenderer spriteRenderer;

    protected override void Awake() {
        base.Awake();
        gameObject.layer = CollisionLayer.Recordable;
    }

    protected override void Start() {
        base.Start();
        spriteRenderer = GetComponent<SpriteRenderer>();
        HighLightMask = transform.Find("HightLightMask").GetComponent<SpriteRenderer>();
        SoundManager.Post(PropertySFXs[Property], gameObject);
    }         

    protected override void Update() {
        base.Update();
        if (Player.Target == this) {
            HighLightMask.color = Color.Lerp(HighLightMask.color, Property == Player.Selected.Property ? HightLightGreen : HighLightRed, Time.deltaTime * 12f);
        }
        else {
            HighLightMask.color = Color.Lerp(HighLightMask.color, Transparent, Time.deltaTime * 12f);
        }        
    }

    public virtual void Expire() {
        SoulExplosion.Instantiate(transform.position, Property);
        ChasingSoul.Instantiate(transform.position,Property);        
        SoundManager.Post(Collection.StopElementals, gameObject);
        Destroy(gameObject);
        Player.Target = null;
    }

}
