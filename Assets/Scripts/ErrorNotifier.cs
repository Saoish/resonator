﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ErrorNotifier : MonoBehaviour {
    public static ErrorNotifier instance;
    private Text message;
    private Image bg;
    private bool Pushing = false;    
	void Start () {
        instance = this;
        message = GetComponentInChildren<Text>();
        bg = GetComponent<Image>();
	}
	
    public static void Push(string message) {
        SoundManager.Post(Collection.Error, instance.gameObject);
        instance.message.text = message;        
        instance.StopAllCoroutines();
        instance.StartCoroutine(instance.YieldFade());
    }

    IEnumerator YieldFade() {
        Pushing = true;
        yield return new WaitForSeconds(2f);
        Pushing = false;
    }

	// Update is called once per frame
	void Update () {
        Color msgColor = message.color;
        msgColor.a = Mathf.Lerp(msgColor.a, Pushing == this ? 1f : 0f, Time.deltaTime * 2f);
        message.color = msgColor;

        Color bgColor = bg.color;
        bgColor.a = Mathf.Lerp(bgColor.a, Pushing == this ? 1f : 0f, Time.deltaTime * 2f);
        bg.color = bgColor;
    }
}
