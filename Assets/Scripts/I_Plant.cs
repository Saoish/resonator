﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class I_Plant : Interactable {
    public Transform Plant;    
    public GameObject WaterSpray;
    Rigidbody2D rb;

    protected override void Awake() {
        base.Awake();
        rb = GetComponent<Rigidbody2D>();
    }

    public override void Activate() {        
        WaterSpray.SetActive(true);
        StartCoroutine(TakeEffect());
    }

    IEnumerator TakeEffect() {
        SoundManager.Post(Collection.WaterSpray, gameObject);
        yield return new WaitForSeconds(5f);
        SoundManager.Post(Collection.GrowthPlant, gameObject);
        float Counter = 0f;
        float Duration = 10f;

        Camera.main.GetComponent<Animator>().Play("camera_shake");
        yield return new WaitForSeconds(2f);

        while (Counter < Duration) {
            Counter += Time.deltaTime;
            rb.mass += 100f;
            rb.drag += 100f;
            Plant.transform.localScale = Vector2.Lerp(Plant.transform.localScale, new Vector2(50f, 50f), Counter / Duration);
            yield return new WaitForEndOfFrame();
        }        
    }

    public override bool ResonatingWith(Property p) {
        return p == Property.Water;
    }        
}
