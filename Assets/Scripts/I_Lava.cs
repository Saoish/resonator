﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class I_Lava : MonoBehaviour {
    public GameObject LavaLoop;
    public ZippyWater2D Lava;
    public ZippyWater2D Rainbow;    

	// Use this for initialization
	void Start () {
        gameObject.tag = Tag.Deadly;
        SoundManager.Post(Collection.LavaLoop, LavaLoop);
	}

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == Tag.Player)
            SoundManager.Post(Collection.PlayerLavaEnter, collider.gameObject);
        else if(collider.tag == Tag.RubberDuck)
            SoundManager.Post(Collection.RubberDuckLavaEnter, collider.gameObject);
    }
    
    public void TurnIntoRainbow() {
        StartCoroutine(ToRainBow());                
    }

    IEnumerator ToRainBow() {
        float Counter = 0f;
        float Duration = 1f;
        SoundManager.Post(Collection.StopLavaLoop, LavaLoop);
        //while (Counter < Duration) {
        //    Counter += Time.deltaTime;
        //    Lava.height = Mathf.Lerp(Lava.height, 0f, Counter / Duration);
        //    yield return new WaitForEndOfFrame();
        //}        
        Counter = 0f;
        Lava.gameObject.SetActive(false);
        Rainbow.gameObject.SetActive(true);
        
        while (Counter < Duration) {
            Counter += Time.deltaTime;
            Rainbow.height = Mathf.Lerp(Rainbow.height, 8f, Counter / Duration);
            yield return new WaitForEndOfFrame();
        }
    }
}
