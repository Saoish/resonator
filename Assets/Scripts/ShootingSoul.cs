﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingSoul : Soul {
    bool Extinguished = false;
    public static void Instantiate(Vector2 position, Elemental.Property Property) {
        ShootingSoul ss = (Instantiate(Resources.Load("ShootingSoul"), position, Quaternion.identity) as GameObject).GetComponent<ShootingSoul>();
        ss.Property = Property;
    }

    protected override void Awake() {
        base.Awake();        
    }

    protected override void Start() {
        base.Start();
        Vector2 Direction = MyCursor.Position - player.Position;
        rb.velocity = Direction.normalized * speed ;
        SoundManager.Post(Collection.SoulSummon, gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.layer == CollisionLayer.Recordable || collider.tag == Tag.Player || collider.tag == Tag.Boss) {            
            return;
        }
        else if (collider.gameObject.layer != CollisionLayer.Interactable){                        
            Extinguish();
        }
    }

    public void Jackpot(bool ExplodeSFX = true) {
        if (Extinguished)
            return;
        Extinguished = true;
        if (ExplodeSFX)
            SoundManager.Post(Collection.SoulExplosion, gameObject);
        else
            SoundManager.Post(Collection.StopSoulLoop, gameObject);
        StartCoroutine(LightUpAndDown(100f));
        GetComponent<Collider2D>().enabled = false;
        ps.startSpeed = 0f;
        ps.emissionRate = 0f;
        rb.velocity /= 5f;
        sub_emit.startSpeed = 5f;
        sub_emit.gameObject.SetActive(true);
        DestroyObject(sub_emit.gameObject, 3f);
        DestroyObject(gameObject, 3f);
    }

    public void Extinguish() {
        if (Extinguished)
            return;
        Extinguished = true;
        SoundManager.Post(Collection.SummonFailure, gameObject);
        StartCoroutine(LightUpAndDown(50f));
        GetComponent<Collider2D>().enabled = false;
        ps.startSpeed = 0f;
        ps.emissionRate = 0f;
        rb.velocity/=5f;
        sub_emit.startSpeed = 1f;
        sub_emit.gameObject.SetActive(true);
        DestroyObject(sub_emit.gameObject, 3f);
        DestroyObject(gameObject, 3f);
    }
    
}
