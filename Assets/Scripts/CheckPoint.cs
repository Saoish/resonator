﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {    
    public bool Activated = false;
    public enum Entity {
        Friendly,
        Enemy
    }
    public Entity entity;
    public int Index;

    private ParticleSystem ActivationEffect;
    private ParticleSystem OnGoingEffect;
    
    public Vector2 SpawnPositon { get { return (Vector2)transform.position + new Vector2(0f, 5f); } }

    public Elemental.Property[] ProvidedAbilities;


    // Use this for initialization
    void Start () {
        ParticleSystem[] PSs = GetComponentsInChildren<ParticleSystem>(true);
        ActivationEffect = PSs[0];
        OnGoingEffect = PSs[1];
        if (entity == Entity.Friendly) {
            ActivationEffect.startColor = OnGoingEffect.startColor = new Color(0.6f, 0.655f, 1f, 0.588f);
        }
        else {
            ActivationEffect.startColor = OnGoingEffect.startColor = new Color(1f, 0.251f, 0.251f, 0.588f);
        }
        if (Activated) {
            SoundManager.Post(Collection.CheckPointLoop, gameObject);
            OnGoingEffect.gameObject.SetActive(true);
            GetComponent<Collider2D>().enabled = false;
        }
    }        

    public void PreActivate() {  
        Activated = true;        
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == Tag.Player)
            StartCoroutine(Activate());        
    }
    
    IEnumerator Activate() {
        if (entity == Entity.Enemy) {
            foreach (var EP in GameManager.Player.GetComponentsInChildren<ElementalButton>(true)) {
                EP.WipeLoad();
                SoundManager.Post(Collection.StartBossBGM, GameManager.Player.gameObject);
            }
        }
        GameManager.UpdateCheckPoint(this);
        GetComponent<Collider2D>().enabled = false;
        Activated = true;
        SoundManager.Post(Collection.CheckPointActivation, gameObject);
        SoundManager.Post(Collection.CheckPointLoop, gameObject);
        ActivationEffect.gameObject.SetActive(true);        
        yield return new WaitForSeconds(0.5f);        
        OnGoingEffect.gameObject.SetActive(true);
    }
}
