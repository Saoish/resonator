﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ElementalButton : MonoBehaviour, ISelectHandler, IDeselectHandler, ISubmitHandler {    
    Color DisableColor = new Color(0.4f, 0.4f, 0.4f);
    Color EnableColor = Color.white;
    public Text Key;
    public Image HighLight;
    public Image Icon;
    public Text LoadText;
    public Image FillMask;
    [HideInInspector]
    public Elemental.Property Property;
    [HideInInspector]
    public int Load = 0;

    Player player;

    private bool PropertyLoaded = false;

    public static ElementalButton Instantiate(Transform parent,Elemental.Property property) {//A transform contains gird layerout control
        ElementalButton eb = (Instantiate(Resources.Load("ElementalButton"), parent) as GameObject).GetComponent<ElementalButton>();        
        eb.Property = property;
        eb.PropertyLoaded = true; 
        return eb;
    }
    private void Awake() {
        DefaultLocalScale = transform.localScale;        
    }
    private void Start() {
        StartCoroutine(SettingUp());
    }IEnumerator SettingUp() {
        while (!PropertyLoaded)
            yield return null;
        Icon.sprite = Resources.Load<Sprite>("Icons/" + Property.ToString());
        LoadText.text = Load.ToString();
        player = GetComponentInParent<Player>();
        Key.text = ((int)Property + 1).ToString();
    }

    private void Update() {             
        Icon.color = Color.Lerp(Icon.color, Load > 0 ? EnableColor : DisableColor, Time.deltaTime * 12f);
        Color frameColor = HighLight.color;
        frameColor.a = Mathf.Lerp(frameColor.a, Player.Selected == this ? 1f : 0f, Time.deltaTime * 6f);
        HighLight.color = frameColor;
    }

    public void AddLoad() {
        LoadText.text = (++Load).ToString();
    }

    public void WipeLoad() {
        LoadText.text = (Load = 0).ToString();
        if(transform.parent.parent.GetComponentInChildren<TopNotifier>()!=null)
            transform.parent.parent.GetComponentInChildren<TopNotifier>().Push("An unknown power has taken away all you collected sound souls.");
    }

    public void TakeAction() {
        if (Player.Target is Recordable)
            StartRecording();
        else if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent<MyButton>() != null)
            return;
        else if (!Player.Controllable)
            return;
        else
            Shoot();
    }

    public void Shoot() {
        if (Load == 0)
            ErrorNotifier.Push("You haven't collected this sound soul yet");
        else
            ShootingSoul.Instantiate(player.Position, Property);
    }

    Coroutine RecordingCO = null;
    public void StopRecording() {
        player.Recording = false;
        SoundManager.Post(Collection.StopRecord, player.gameObject);
        player.MPUI.DisableRecordingText();
        if (RecordingCO != null)
            StopCoroutine(RecordingCO);
        FillMask.fillAmount = 0f;
    }
    public void StartRecording() {
        if(((Recordable)Player.Target).Property != this.Property){
            ErrorNotifier.Push("This sound soul doesn't like your resonator");
            return;
        }
            
        if (RecordingCO != null) {
            StopCoroutine(RecordingCO);
        }
        player.Recording = true;        
        SoundManager.Post(Collection.StartRecord, player.gameObject);
        player.MPUI.EnableRecordingText();        
        RecordingCO = StartCoroutine(YildRecording());
    }IEnumerator YildRecording() {
        float Counter = 0;
        while (Counter < Recordable.timeRequired) {
            if (Player.Target==null || ((Recordable)Player.Target).Property!=Player.Selected.Property) {
                StopRecording();                
                yield break;
            }
            Counter += Time.deltaTime;
            float timeProgressed = Counter / Recordable.timeRequired;
            FillMask.fillAmount = timeProgressed;
            yield return new WaitForEndOfFrame();
        }        
        LoadText.text = Load.ToString();
        StopRecording();
        ((Recordable)Player.Target).Expire();
        RecordingCO = null;
    }
    

    public Elemental.Property Next() {
        int nextIndex = (int)this.Property + 1;
        return nextIndex == Elemental.NumOfProperties ? 0 : (Elemental.Property)nextIndex;        
    }

    public Elemental.Property Prev() {
        int prevIndex = (int)this.Property - 1;
        return prevIndex < 0 ? (Elemental.Property)(Elemental.NumOfProperties-1) : (Elemental.Property)prevIndex;
    }

    public void OnSelect(BaseEventData eventData) {
        ClearUICOs();
        MagnifyCO = StartCoroutine(Magnify());
        MyCursor.UpdateProperty();
        SoundManager.Post(Collection.ButtonSelect,gameObject);
    }

    public void OnDeselect(BaseEventData eventData) {
        ClearUICOs();
        ResizeCO = StartCoroutine(Resize());
    }

    public void OnSubmit(BaseEventData eventData) {        
        ClearUICOs();        
        BounceCO = StartCoroutine(Bounce());
    }


    //helper
    protected float ShrinkFactor = 0.8f;    
    protected Vector2 DefaultLocalScale = new Vector2(1f, 1f);
    Coroutine BounceCO = null;
    IEnumerator Bounce() {
        float LerpTime = 0.1f;
        float Counter = 0;
        Vector2 ShrinkV2 = DefaultLocalScale * ShrinkFactor;
        while (Counter < LerpTime) {
            Counter += Time.deltaTime;
            float timeProgressed = Counter / LerpTime;
            transform.localScale = Vector2.Lerp(transform.localScale, ShrinkV2, timeProgressed);
            yield return new WaitForEndOfFrame();
        }
        Counter = 0;
        Vector2 MagnifyV2 = DefaultLocalScale * MagnifyFactor;
        while (Counter < LerpTime) {
            Counter += Time.deltaTime;
            float timeProgressed = Counter / LerpTime;
            transform.localScale = Vector2.Lerp(transform.localScale, MagnifyV2, timeProgressed);
            yield return new WaitForEndOfFrame();
        }
        BounceCO = null;
    }

    protected float MagnifyFactor = 1.4f;
    Coroutine MagnifyCO = null;
    IEnumerator Magnify() {
        float LerpTime = 0.1f;
        float Counter = 0;
        Vector2 MagnifyV2 = DefaultLocalScale * MagnifyFactor;
        while (Counter < LerpTime) {
            Counter += Time.deltaTime;
            float timeProgressed = Counter / LerpTime;
            transform.localScale = Vector2.Lerp(transform.localScale, MagnifyV2, timeProgressed);
            yield return new WaitForEndOfFrame();
        }
    }

    Coroutine ResizeCO = null;
    IEnumerator Resize() {
        float LerpTime = 0.1f;
        float Counter = 0;
        while (Counter < LerpTime) {
            Counter += Time.deltaTime;
            float timeProgressed = Counter / LerpTime;
            transform.localScale = Vector2.Lerp(transform.localScale, DefaultLocalScale, timeProgressed);
            yield return new WaitForEndOfFrame();
        }
    }

    protected void ClearUICOs() {
        if (BounceCO != null)
            StopCoroutine(BounceCO);
        if (MagnifyCO != null)
            StopCoroutine(MagnifyCO);
        if (ResizeCO != null)
            StopCoroutine(ResizeCO);
    }
}
