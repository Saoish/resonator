﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MyButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {        

    public virtual void OnPointerEnter(PointerEventData eventData) {        
        SoundManager.Post(Collection.ButtonHover, gameObject);
        ClearCOs();
        MagnifyCO = StartCoroutine(Magnify());        
    }

    public virtual void OnPointerExit(PointerEventData eventData) {
        ClearCOs();
        ResizeCO = StartCoroutine(Resize());
    }


    public virtual void OnPointerClick(PointerEventData eventData) {        
        SoundManager.Post(Collection.ButtonSubmit, gameObject);
        ClearCOs();
        BounceCO = StartCoroutine(Bounce());
    }


    Coroutine BounceCO = null;
    IEnumerator Bounce() {
        float LerpTime = 0.1f;
        float Counter = 0;
        Vector2 ShrinkV2 = DefaultLocalScale * ShrinkFactor;
        while (Counter < LerpTime) {
            Counter += Time.deltaTime;
            float timeProgressed = Counter / LerpTime;
            transform.localScale = Vector2.Lerp(transform.localScale, ShrinkV2, timeProgressed);
            yield return new WaitForEndOfFrame();
        }
        Counter = 0;
        Vector2 MagnifyV2 = DefaultLocalScale * MagnifyFactor;
        while (Counter < LerpTime) {
            Counter += Time.deltaTime;
            float timeProgressed = Counter / LerpTime;
            transform.localScale = Vector2.Lerp(transform.localScale, MagnifyV2, timeProgressed);
            yield return new WaitForEndOfFrame();
        }
        BounceCO = null;
    }

    Coroutine MagnifyCO = null;
    IEnumerator Magnify() {
        float LerpTime = 0.1f;
        float Counter = 0;
        Vector2 MagnifyV2 = DefaultLocalScale * MagnifyFactor;
        while (Counter < LerpTime) {
            Counter += Time.deltaTime;
            float timeProgressed = Counter / LerpTime;
            transform.localScale = Vector2.Lerp(transform.localScale, MagnifyV2, timeProgressed);
            yield return new WaitForEndOfFrame();
        }
    }

    Coroutine ResizeCO = null;
    IEnumerator Resize() {
        float LerpTime = 0.1f;
        float Counter = 0;
        while (Counter < LerpTime) {
            Counter += Time.deltaTime;
            float timeProgressed = Counter / LerpTime;
            transform.localScale = Vector2.Lerp(transform.localScale, DefaultLocalScale, timeProgressed);
            yield return new WaitForEndOfFrame();
        }
    }

    protected void ClearCOs() {
        if (BounceCO != null)
            StopCoroutine(BounceCO);
        if (MagnifyCO != null)
            StopCoroutine(MagnifyCO);
        if (ResizeCO != null)
            StopCoroutine(ResizeCO);
    }

    protected float CommonFadeDuration = 0.5f;
    protected float ShrinkFactor = 0.8f;
    protected float MagnifyFactor = 1.2f;
    protected Vector2 DefaultLocalScale = new Vector2(1f, 1f);
    protected float FadeFactor = 0.3f;
    protected float DefaultAlpha;
}
