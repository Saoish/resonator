﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour {    
    public static bool Controllable = false;

    int DefaultSpeed = 800;
    [HideInInspector]
    public float JumpPower = 40000f;    
    public Rigidbody2D rb;    
    
    public Vector2 Position { get { return rb.position; } set { rb.position = value; } }
    public Vector2 Force { get { return rb.velocity; } set { rb.velocity = value; } }    
    public bool HasForce { get { return rb.velocity.magnitude > 0f; } }
    
    public bool Grounded = false;
    public bool Recording = false;
    public bool Alived = true;

    private float CurrentSpeed;
    private float GroundedDistance = 2.5f;

    public static Elemental Target;

    public Animator EndgameAnim;

    public Animator Anim;
    
    public PhysicsMaterial2D Concrete;
    public PhysicsMaterial2D Ice;
    public PhysicsMaterial2D Rubber;
    public PhysicsMaterial2D Metal;
    public PhysicsMaterial2D Pot;
    public PhysicsMaterial2D Leaf;

    public PhysicsMaterial2D CurrentPhyMat;

    public MainPlayerUI MPUI;

    [HideInInspector]
    public Elemental.Property[] Collected = null;

    Dictionary<Elemental.Property, ElementalButton> ElementalPointers = new Dictionary<Elemental.Property, ElementalButton>();
    public static ElementalButton Selected;

	void Start () {        
        CurrentSpeed = DefaultSpeed;        
        foreach (Elemental.Property p in System.Enum.GetValues(typeof(Elemental.Property)))
            ElementalPointers[p] = ElementalButton.Instantiate(MPUI.Buttons,p);
        Selected = ElementalPointers[Elemental.Property.Ice];
        PointerEventData ped = new PointerEventData(EventSystem.current);
        Selected.OnSelect(ped);
        StartCoroutine(Spawnning());
    }

    IEnumerator Spawnning() {
        Controllable = false;
        rb.bodyType = RigidbodyType2D.Kinematic;
        while (Collected==null)
            yield return null;   
        foreach(var c in Collected) {
            ElementalPointers[c].AddLoad();
        }
        yield return new WaitForSeconds(1f);
        float Counter = 0f;             
        float Duration = 1f;
        while (Counter < Duration) {            
            Counter += Time.deltaTime;
            Anim.GetComponent<SpriteRenderer>().color = Color.Lerp(Anim.GetComponent<SpriteRenderer>().color, Color.white, Counter/Duration);
            yield return new WaitForEndOfFrame();
        }        
        Anim.GetComponent<SpriteRenderer>().color = Color.white;
        rb.bodyType = RigidbodyType2D.Dynamic;
        Controllable = true;
    }

    void Update() {
        if (!Alived || !Controllable) {            
            return;
        }
        if (Grounded && Input.GetKeyDown(KeyCode.Space)) {
            rb.AddForce(Vector2.up * JumpPower);            
            Anim.Play("char_jump", 1, 0f);
            PlayFootStepSFX();
        }
        if (Input.GetKeyDown(KeyCode.E)) {
            PointerEventData ped = new PointerEventData(EventSystem.current);
            Selected.OnDeselect(ped);
            Selected = ElementalPointers[Selected.Next()];
            Selected.OnSelect(ped);
        }
        else if (Input.GetKeyDown(KeyCode.Q)) {
            PointerEventData ped = new PointerEventData(EventSystem.current);
            Selected.OnDeselect(ped);
            Selected = ElementalPointers[Selected.Prev()];
            Selected.OnSelect(new PointerEventData(EventSystem.current));
        }
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            PointerEventData ped = new PointerEventData(EventSystem.current);
            Selected.OnDeselect(ped);
            Selected = ElementalPointers[(Elemental.Property)0];
            Selected.OnSelect(ped);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            PointerEventData ped = new PointerEventData(EventSystem.current);
            Selected.OnDeselect(ped);
            Selected = ElementalPointers[(Elemental.Property)1];
            Selected.OnSelect(ped);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            PointerEventData ped = new PointerEventData(EventSystem.current);
            Selected.OnDeselect(ped);
            Selected = ElementalPointers[(Elemental.Property)2];
            Selected.OnSelect(ped);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4)) {
            PointerEventData ped = new PointerEventData(EventSystem.current);
            Selected.OnDeselect(ped);
            Selected = ElementalPointers[(Elemental.Property)3];
            Selected.OnSelect(ped);
        }
        //if (Input.GetKeyDown(KeyCode.Alpha5)) {
        //    PointerEventData ped = new PointerEventData(EventSystem.current);
        //    Selected.OnDeselect(ped);
        //    Selected = ElementalPointers[(Elemental.Property)4];
        //    Selected.OnSelect(ped);
        //}
        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            Selected.TakeAction();
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0) && Recording) {
            Selected.StopRecording();
        }
    }

    private void FixedUpdate() {
        _WalkingFixedUpdate();
    }

    private void _WalkingFixedUpdate() {
        if (!Alived || !Controllable) {
            rb.velocity = new Vector2(0f, rb.velocity.y);
            Anim.SetBool("Running", false);
            Anim.SetLayerWeight(1, 0f);
            Anim.SetLayerWeight(0, 1f);
            return;
        }
        float HorizontalMoveAxies = Input.GetAxisRaw("Horizontal");
        if (Grounded && !Recording) {                        
            Anim.SetLayerWeight(1, 0f);
            Anim.SetLayerWeight(0, 1f);
            if (HorizontalMoveAxies != 0) {
                Anim.transform.localScale = HorizontalMoveAxies > 0 ? new Vector2(1f, 1f) : new Vector2(-1f, 1f);
                Anim.SetBool("Running", true);
            }
            else
                Anim.SetBool("Running", false);                           
            if (CurrentPhyMat == Ice)                                    
                rb.AddForce(new Vector2(HorizontalMoveAxies, 0f) * CurrentSpeed / 5f);
            else
                rb.velocity = new Vector2(HorizontalMoveAxies, 0f) * CurrentSpeed * Time.fixedDeltaTime;
        }        
        else {            
            if(!Grounded) {
                Anim.SetLayerWeight(0, 0f);
                Anim.SetLayerWeight(1, 1f);                
                if (Mathf.Abs(rb.velocity.x) < Mathf.Abs((HorizontalMoveAxies * CurrentSpeed / 10f * Time.fixedDeltaTime)))
                        rb.velocity = new Vector2(rb.velocity.x + (HorizontalMoveAxies * CurrentSpeed / 100f * Time.fixedDeltaTime), rb.velocity.y);                
            }                     
        }
    }

    public void Collect(Elemental.Property EP) {
        ElementalPointers[EP].AddLoad();
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == Tag.Deadly) {
            if (Alived) {
                Alived = false;
                Controllable = false;
                Anim.enabled = false;
                GameObject Explode = Instantiate(Resources.Load("DieingSparkling"), transform) as GameObject;
                SoundManager.Post(Collection.Die, gameObject);
                SoundManager.Post(Collection.IncreaseLowPass, gameObject);
                EndgameAnim.Play("endgame_pop_fail", 0, 0f);
            }
        }
    }

    
    
    public static void PlayFootStepSFX() {
        SoundManager.Post(Collection.RobotMove, GameManager.Player.gameObject);
        if (GameManager.Player.CurrentPhyMat == GameManager.Player.Concrete)
            SoundManager.Post(Collection.FootStepConcrete, GameManager.Player.gameObject);
        else if (GameManager.Player.CurrentPhyMat == GameManager.Player.Ice)
            SoundManager.Post(Collection.FootStepIce, GameManager.Player.gameObject);
        else if (GameManager.Player.CurrentPhyMat == GameManager.Player.Rubber)
            SoundManager.Post(Collection.FootStepRubber, GameManager.Player.gameObject);                    
        else if (GameManager.Player.CurrentPhyMat == GameManager.Player.Metal)
            SoundManager.Post(Collection.FootStepMetal, GameManager.Player.gameObject);
        else if (GameManager.Player.CurrentPhyMat == GameManager.Player.Pot)
            SoundManager.Post(Collection.PotKicking, GameManager.Player.gameObject);
        else if (GameManager.Player.CurrentPhyMat == GameManager.Player.Leaf)
            SoundManager.Post(Collection.FootStepLeaf, GameManager.Player.gameObject);
    }
}
