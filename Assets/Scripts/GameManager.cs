﻿using UnityEngine;

public class GameManager : MonoBehaviour {    
    public static GameManager instance;

    public static Player Player;
    public static Boss Boss;    

    public static CheckPoint[] CheckPoints;
    public static int LastCheckPointIndex = 0;

    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);                 
        }
        else {
            instance = this;
            DontDestroyOnLoad(this);            
            CollisionLayer.SetUpCollisionsIgnorance();            
        }
        SetUp();
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }

    // Use this for initialization
    void Start () {
    }

    public void SetUp() {
        Player = FindObjectOfType<Player>();
        Boss = FindObjectOfType<Boss>();
        CheckPoints = FindObjectsOfType<CheckPoint>();        
        for (int i = 0; i < CheckPoints.Length; i++) {
            if (CheckPoints[i].Index < LastCheckPointIndex)
                CheckPoints[i].PreActivate();
            else if (CheckPoints[i].Index == LastCheckPointIndex) {
                Player.Collected = CheckPoints[i].ProvidedAbilities;
                Player.Position = CheckPoints[i].SpawnPositon;                
                Destroy(Instantiate(Resources.Load("SpawningEffect"), Player.Position, Quaternion.identity) as GameObject, 10f);                
            }
        }        
    }
	
    public void ToCheckPoint() {
        SoundManager.Post(Collection.HardReset, Player.gameObject);
        Application.LoadLevel(0);        
    }

    public void Quit() {
        Application.Quit();
    }

    public void Restart() {
        LastCheckPointIndex = 0;
        ToCheckPoint();
    }

    public static void UpdateCheckPoint(CheckPoint CP) {
        LastCheckPointIndex = CP.Index;                   
    }
}
