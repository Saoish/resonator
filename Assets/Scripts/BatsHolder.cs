﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatsHolder : MonoBehaviour {

    private void OnEnable() {
        SoundManager.Post(Collection.BatsFlyingOver, gameObject);
    }
}
