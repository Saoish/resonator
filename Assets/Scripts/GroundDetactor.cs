﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetactor : MonoBehaviour {
    Player Player;
    public Collider2D[] ignores;
    
	void Start () {
        Player = GetComponentInParent<Player>();
        Collider2D self = GetComponent<Collider2D>();
        foreach(var c in ignores) {
            Physics2D.IgnoreCollision(self, c);
        }
	}

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.layer == CollisionLayer.Ground) {
            if (collider.sharedMaterial == Player.Rubber) {
                Player.rb.mass = 0f;
                Player.JumpPower = 0.2f;
            }
            else {
                Player.rb.mass = 20f;
                Player.JumpPower = 40000f;
            }
            Player.CurrentPhyMat = collider.sharedMaterial;
            Player.Grounded = true;
        }
        else {
            Player.rb.mass = 20f;
            Player.JumpPower = 40000f;
        }
        Player.PlayFootStepSFX();
    }

    private void OnTriggerStay2D(Collider2D collider) {        
        if(collider.gameObject.layer == CollisionLayer.Ground) {
            if (collider.sharedMaterial == Player.Rubber) {                
                Player.rb.mass = 0f;
                Player.JumpPower = 0.2f;                
            }
            else {                
                Player.rb.mass = 20f;
                Player.JumpPower = 40000f;
            }
            Player.CurrentPhyMat = collider.sharedMaterial;
            Player.Grounded = true;                                   
        }
        else {            
            Player.rb.mass = 20f;
            Player.JumpPower = 40000f;
        }
    }

    private void OnTriggerExit2D(Collider2D collider) {
        if (collider.gameObject.layer == CollisionLayer.Ground) {
            Player.Grounded = false;
        }
    }

}
