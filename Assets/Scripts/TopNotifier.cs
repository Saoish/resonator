﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopNotifier : MonoBehaviour {    
    public Text message;
    public CanvasGroup CG;                   
    private bool Pushing = false;

    public void Push(string message) {
        this.message.text = message;
        StopAllCoroutines();
        StartCoroutine(YieldFade());
    }

    IEnumerator YieldFade() {
        Pushing = true;
        yield return new WaitForSeconds(5f);
        Pushing = false;
    }

    // Update is called once per frame
    void Update() {
        CG.alpha = Mathf.Lerp(CG.alpha, Pushing ? 1f : 0f, Time.deltaTime * 6f);
    }
}
