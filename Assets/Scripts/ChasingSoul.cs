﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasingSoul : Soul {
    
    private bool Following = true;
    bool Expired = false;    
    
    public static void Instantiate(Vector2 position, Elemental.Property Property) {
        ChasingSoul ee = (Instantiate(Resources.Load("ChasingSoul"), position, Quaternion.identity) as GameObject).GetComponent<ChasingSoul>();        
        ee.Property = Property;        
    }

	protected override void Start () {
        base.Start();
        rb.velocity = (Quaternion.Euler(0,0,Random.Range(0f,360f))*Vector2.right) * speed;        
    }
    
    void FixedUpdate () {
        if (Following) {            
            Vector2 toTarget = (player.Position - (Vector2)transform.position).normalized;
            rb.velocity = Vector3.RotateTowards(rb.velocity, toTarget, 180f * Mathf.Deg2Rad * Time.fixedDeltaTime, 0);
        }
        else {
            rb.velocity = Vector2.zero;
            rb.position = Vector2.Lerp(rb.position, player.Position,Time.fixedDeltaTime*12f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Player") {            
            Expire();
        }            
    }

    private void Expire() {
        if (Expired)
            return;
        Expired = true;
        player.Collect(Property);
        StartCoroutine(LightUpAndDown(100f));
        SoundManager.Post(Collection.SoulExplosion, gameObject);
        GetComponent<Collider2D>().enabled = false;
        Following = false;        
        ps.startSpeed = 0f;
        ps.emissionRate = 0f;
        sub_emit.transform.SetParent(player.transform);        
        sub_emit.transform.localPosition = Vector2.zero;
        sub_emit.gameObject.SetActive(true);
        DestroyObject(sub_emit.gameObject, 3f);
        DestroyObject(gameObject, 3f);
    }

}
