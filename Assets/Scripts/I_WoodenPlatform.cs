﻿using UnityEngine;

public class I_WoodenPlatform : Interactable {
    public Animator Anim;
    public override void Activate() {
        SoundManager.Post(Collection.BurnOffWoodenPlatform, gameObject);        
        Anim.Play("wooden_platform_burnoff");
    }

    public override bool ResonatingWith(Property p) {
        return p == Property.Fire;
    }
}
