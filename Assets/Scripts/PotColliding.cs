﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotColliding : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision) {
        SoundManager.Post(Collection.PotKicking, gameObject);
    }
}
