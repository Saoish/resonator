﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CollisionLayer {
    //Predefined
    public static int Default = LayerMask.NameToLayer("Default");
    public static int IgnoreCast = LayerMask.NameToLayer("Ignore Raycast");

    //Unique
    public static int Ground = LayerMask.NameToLayer("Ground");
    public static int Interactable = LayerMask.NameToLayer("Interactable");
    public static int Recordable = LayerMask.NameToLayer("Recordable");
    public static int Soul = LayerMask.NameToLayer("Soul");
    public static int Effector = LayerMask.NameToLayer("Effector");
    public static int CheckPoint = LayerMask.NameToLayer("CheckPoint");

    public static void SetUpCollisionsIgnorance() {
        Physics2D.IgnoreLayerCollision(Soul, Soul);
        Physics2D.IgnoreLayerCollision(Soul, Effector);
        Physics2D.IgnoreLayerCollision(Soul, CheckPoint);
    }
}
