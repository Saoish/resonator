﻿using UnityEngine;

public class SoundManager : MonoBehaviour {
    public static SoundManager instance;
    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }

    public static void Post(string Event, GameObject GO) {        
        AkSoundEngine.PostEvent(Event, GO);
    }

    public static float GetRTPC(string RTPC,GameObject GO) { // doesn't work
        float value;
        int type = (int)RTPCValue_type.RTPCValue_GameObject;
        uint pid = (uint)RTPCValue_type.RTPCValue_PlayingID;
        AkSoundEngine.GetRTPCValue("Spinning", GO, pid, out value, ref type);
        return value;
    }
}

public static class RTPC {
    public static string SpinningSpeed = "SpinningSpeed";
}

public static class Collection {    
    //Effects
    public static string Wind = "Wind";
    public static string Flame = "Flame";
    public static string Stream = "Stream";
    public static string Blizzard = "Blizzard";
    public static string ElectricLoop = "ElectricLoop";

    public static string StopElementals = "StopElementals";

    public static string SoulLoop = "SoulLoop";
    public static string StopSoulLoop = "StopSoulLoop";
    public static string SoulExplosion = "SoulExplosion";
    public static string SoulSummon = "SoulSummon";
    public static string SummonFailure = "SummonFailure";

    public static string PlayerWaterEnter = "PlayerWaterEnter";
    public static string WaterSplashEnter = "WaterSplashEnter";
    public static string WaterSplashExit = "WaterSplashExit";
    public static string UnderWaterLoop = "UnderWaterLoop";

    public static string PlayerLavaEnter = "PlayerLavaEnter";

    public static string LavaLoop = "LavaLoop";
    public static string LavaSplashEnter = "LavaSplashEnter";
    public static string LavaSplashExit = "LavaSplashExit";
    public static string StopLavaLoop = "StopLavaLoop";
    public static string RubberDuckLavaEnter = "RubberDuckLavaEnter";

    public static string BurnOffWoodenPlatform = "BurnOffWoodenPlatform";
    public static string ElectricArcing = "ElectricArcing";
    public static string HelicopterStart = "HelicopterStart";

    public static string Frozen = "Frozen";
    public static string PotKicking = "PotKicking";
    public static string WaterSpray = "WaterSpray";
    public static string GrowthPlant = "GrowthPlant";

    public static string CheckPointActivation = "CheckPointActivation";
    public static string CheckPointLoop = "CheckPointLoop";

    //Player Behaviors
    public static string StartRecord = "StartRecord";
    public static string StopRecord = "StopRecord";
    public static string Footstep = "Footstep";
    public static string MetalCrash = "MetalCrash";
    public static string Die = "Die";
    public static string RobotMove = "RobotMove";

    public static string FootStepConcrete = "FootStepConcrete";
    public static string FootStepIce = "FootStepIce";
    public static string FootStepRubber = "FootStepRubber";
    public static string FootStepMetal = "FootStepMetal";
    public static string FootStepLeaf = "FootStepLeaf";

    public static string PlayerHurt = "PlayerHurt";

    //Boss
    public static string BossAwake = "BossAwake";
    public static string BossAttackEnter = "BossAttackEnter";
    public static string BossAttackExit = "BossAttackExit";
    public static string BossWalk = "BossWalk";
    public static string BossBurning = "BossBurning";
    public static string WaterPuttingOutFire = "WaterPuttingOutFire";
    public static string BossFrozen = "BossFrozen";
    public static string BossIceCrack = "BossIceCrack";
    public static string LightingStrike = "LightingStrike";
    public static string BossExplode = "BossExplode";

    //UI
    public static string ButtonSelect = "ButtonSelect";
    public static string Error = "Error";
    public static string ButtonHover = "ButtonHover";
    public static string ButtonSubmit = "ButtonSubmit";

    //Bus
    public static string IncreaseLowPass = "IncreaseLowPass";
    public static string ResetMainBusLowPass = "ResetMainBusLowPass";

    public static string DecreaseBusPitch = "DecreaseBusPitch";
    public static string ResetBusPitch = "ResetBusPitch";

    public static string HardReset = "HardReset";

    //BGM
    public static string StartBossBGM = "StartBossBGM";
    public static string ShiftTopEndGame = "ShiftTopEndGame";

    //Environment
    public static string BatsFlyingOver = "BatsFlyingOver";    
}
