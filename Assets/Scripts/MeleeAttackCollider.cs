﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackCollider : MonoBehaviour {

    Boss boss;
    Player player;
    BoxCollider2D _collider;
    public Coroutine ScalingCO;
    [HideInInspector]
    public bool Hit = false;
    private void Start() {
        boss = GetComponentInParent<Boss>();
        player = GameManager.Player;
        _collider = GetComponent<BoxCollider2D>();
        Physics2D.IgnoreCollision(_collider, boss.GetComponent<Collider2D>());
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.tag == Tag.Player && !Hit) {
            Hit = true;            
        }
    }

    private void Update() {
        transform.localScale = Vector2.Lerp(transform.localScale, Hit ? new Vector2(3f, 3f) : Vector2.one,Time.deltaTime*12f) ;
    }

    
}
