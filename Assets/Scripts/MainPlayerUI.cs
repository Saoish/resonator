﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPlayerUI : MonoBehaviour {//MainPlayer UI
    public Text RecordingText;
    public Transform Buttons;    
    public void EnableRecordingText() {
        RecordingText.gameObject.SetActive(true);
    }

    public void DisableRecordingText() {
        RecordingText.gameObject.SetActive(false);
    }
}
