﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Light))]

public class LightFlicker : MonoBehaviour {
    [Range(4,50)]
    public float Intensity;
    [Range(0, 10)]
    public float Variance;
    [Range(0, 5)]
    public float Frquency = 4;//paulses/sec    

    public Light Light;
    private float flickCounter = 0;

    void Start() {
        Light = GetComponent<Light>();
        if (Intensity - Variance < 0) {
            Debug.LogError("Invalid Intensity | Variance.");
        }
    }

    void Update() {
        if (flickCounter >= (1/Frquency) * Time.deltaTime) {
            Light.intensity = Random.Range(Intensity + Variance, Intensity - Variance);
            flickCounter = 0;
        }
        else {
            flickCounter += Time.deltaTime;
        }        
    }
}
