﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulExplosion : MonoBehaviour {
    public ParticleSystem sp; 
    public static void Instantiate(Vector2 Position,Elemental.Property EP) {
        SoulExplosion SE = (Instantiate(Resources.Load("SoulExplosion"), Position, Quaternion.identity) as GameObject).GetComponent<SoulExplosion>();
        SE.sp.startColor = Elemental.Colors[EP];
    }
	
	void Start () {
        SoundManager.Post(Collection.SoulExplosion, gameObject);
        Destroy(gameObject, 2f);
	}
		
}
