﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : Elemental {
    public SpriteRenderer HighlighMask;
    protected bool Activated = false;
    public abstract bool ResonatingWith(Property p);
    protected override void Start() {
        base.Start();
        gameObject.layer = CollisionLayer.Interactable;
	}

    protected Color HighLight = new Color(1f,1f,0f,0.5f);
    protected Color Normal = new Color(1f, 1f, 0f, 0f);

    protected override void Update() {//For most cases
        base.Update();        
        if (!Activated)
            HighlighMask.color = Color.Lerp(HighlighMask.color, Player.Target == this ? HighLight : Normal, Time.deltaTime * 12f);
        else
            HighlighMask.color = Color.Lerp(HighlighMask.color, Normal, Time.deltaTime * 12f);        
    }

    public abstract void Activate();

    protected virtual void OnTriggerEnter2D(Collider2D collider) {
        if(collider.gameObject.layer == CollisionLayer.Soul) {            
            ShootingSoul ss = collider.GetComponent<ShootingSoul>();
            if (ss == null)//Chasing type
                return;
            if (ResonatingWith(ss.Property) && !Activated) {
                ss.Jackpot();
                Activate();
                Activated = true;                
            }
            else {
                ss.Extinguish();
            }
        }
    }
}
