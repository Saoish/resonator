﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatTrigger : MonoBehaviour {
    public Animator Anim;    
    private void OnTriggerEnter2D(Collider2D collider) {
        if(collider.tag == Tag.Player){
            GetComponent<Collider2D>().enabled = false;            
            Anim.Play("flying_over");            
        }
    }
}
