﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tag  {    
    public static string Player = "Player";
    public static string Deadly = "Deadly";
    public static string RubberDuck = "RubberDuck";
    public static string Boss = "Boss";
}
