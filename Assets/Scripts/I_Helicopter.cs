﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class I_Helicopter : Interactable {
    public Animator Anim;
    public GameObject Effector;
    protected override void Start() {
        base.Start();
        SoundManager.Post(Collection.ElectricArcing, gameObject);        
    }

    public override void Activate() {                
        SoundManager.Post(Collection.HelicopterStart, gameObject);        
        Anim.Play("helocopter_start_spinning");
        StartCoroutine(SpeedUpAnim());
    }
    IEnumerator SpeedUpAnim() {
        int SpeedUpTimes = 0;
        while (SpeedUpTimes < 10) {
            yield return new WaitForSeconds(1f);
            Anim.speed += 1;
            SpeedUpTimes++;
            if (SpeedUpTimes == 8) {
                Effector.SetActive(true);
            }
        }        
    }

    protected override void Update() {
        base.Update();        
    }

    public override bool ResonatingWith(Property p) {
        return p == Property.Electric;
    }
}
