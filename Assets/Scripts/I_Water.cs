﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class I_Water : Interactable {    
    public ZippyWater2D ZW;    
    Transform IceGround;
    public override bool ResonatingWith(Property p) {
        return p == Property.Ice;
    }

    protected override void Awake() {
        base.Awake();
        gameObject.tag = Tag.Deadly;
        IceGround = transform.Find("IceGround");
    }

    public override void Activate() {             
        //IceGround.Play("ice_block_freeze");        
        SoundManager.Post(Collection.Frozen, gameObject);
        IceGround.GetComponent<Collider2D>().enabled = true;
        StartCoroutine(Frozen());
        GetComponent<Collider2D>().enabled = false;
    }

    IEnumerator Frozen() {
        float Duration = 100f;
        float Counter = 0f;
        SpriteRenderer[] sps = IceGround.GetComponentsInChildren<SpriteRenderer>();
        Color solid_color = new Color(sps[0].color.r, sps[0].color.g, sps[0].color.b, 1f);
        while (Counter < Duration) {
            Counter += Time.deltaTime;
            foreach(var sp in sps) {
                sp.color = Color.Lerp(sp.color, solid_color, Counter / Duration);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collider) {        
        if(collider.tag == Tag.Player) {            
            SoundManager.Post(Collection.PlayerWaterEnter, collider.gameObject);
            SoundManager.Post(Collection.UnderWaterLoop, collider.gameObject);
            GetComponentInChildren<BuoyancyEffector2D>().density = 1.5f;
        }
        else if (collider.gameObject.layer == CollisionLayer.Soul) {
            ShootingSoul ss = collider.GetComponent<ShootingSoul>();
            if (ss == null)//Chasing type
                return;
            if (ResonatingWith(ss.Property) && !Activated) {
                ss.Jackpot();
                Activate();
                Activated = true;
            }
            else {
                return;
            }
        }
    }
}
