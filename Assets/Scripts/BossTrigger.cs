﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collider) {
        if(collider.tag == Tag.Player) {
            GetComponent<Collider2D>().enabled = false;
            GameManager.Boss.Activate();
        }
    }
}
