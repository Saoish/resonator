﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour {    
    public bool Activated = false;
    public bool Alive = true;
    public Animator Anim;
    private Player Target { get { return GameManager.Player; } }
    public MeleeAttackCollider MeleeAttackCollider;
    public Animator DieAnim;
    public bool InAttackState = false;

    public List<Color> BossStatesColors;
    public List<Color> ParticleStatesColors;
    public GameObject DieSparkling;

    public Animator EndGameAnim;

    public ParticleSystem RootPS;
    public ParticleSystem SubPS;

    private float AttackDistance = 6f;
    private Vector2 MoveVector;
    private Vector2 AttackVector;

    public I_Lava Lava_Rainbow;

    private Rigidbody2D rb;
    public Vector2 Positon { get { return rb.position; } set { rb.position = value; } }

    [HideInInspector]
    public float Speed = 2500f;

    Coroutine FrozenCO;
    


    public Elemental.Property Weak = Elemental.Property.Water;

    private SpriteRenderer BossSprite;

    private void OnTriggerEnter2D(Collider2D collider) {
        if(collider.gameObject.layer == CollisionLayer.Soul) {
            ShootingSoul ss = collider.GetComponent<ShootingSoul>();
            if (!ss)//Chasing
                return;
            if (!Activated || !Alive || ss.Property!=Weak) {
                ss.Extinguish();                
            }
            else {                
                TakeEffect(ss);
            }
        }
    }

    IEnumerator SlowTime() {
        float Counter = 0f;
        float Duration = 1f;        
        SoundManager.Post(Collection.DecreaseBusPitch, gameObject);
        while (Counter< Duration) {
            Counter += Time.deltaTime;
            Time.timeScale = Mathf.Lerp(Time.timeScale, 0.2f, Counter / Duration);
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
            yield return new WaitForEndOfFrame();
        }
        Counter = 0f;
        Duration = 5f;
        SoundManager.Post(Collection.ResetBusPitch, gameObject);
        while (Counter < Duration) {
            Counter += Time.deltaTime;
            Time.timeScale = Mathf.Lerp(Time.timeScale, 1f, Counter / Duration);
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
            yield return new WaitForEndOfFrame();
        }        
    }

    public void TakeEffect(ShootingSoul ss) {
        if(Weak == Elemental.Property.Water) {//State 1            
            ss.Jackpot(false);
            SoundManager.Post(Collection.WaterPuttingOutFire, gameObject);            
            Weak = Elemental.Property.Ice;            
        }else if(Weak == Elemental.Property.Ice) {//State 2            
            ss.Jackpot(false);
            SoundManager.Post(Collection.Frozen, gameObject);
            Weak = Elemental.Property.Electric;
            GetComponent<Collider2D>().sharedMaterial = GameManager.Player.Ice;
            FrozenCO = StartCoroutine(FrozenCoundown());
        }else if(Weak == Elemental.Property.Electric){//State 3
            ss.Jackpot(false);
            if (FrozenCO != null)
                StopCoroutine(FrozenCO);
            StartCoroutine(TakeLightingStrikeEffect());            
        }
    }
    
    IEnumerator TakeLightingStrikeEffect() {
        if (!Alive)
            yield break;
        Alive = false;
        DieSparkling.SetActive(true);
        SoundManager.Post(Collection.LightingStrike, gameObject);
        yield return new WaitForSeconds(1f);                
        StartCoroutine(SlowTime());        
        Destroy(Instantiate(Resources.Load("LightingStrike"), transform.position - new Vector3(0f,4.77f,0f), Quaternion.identity) as GameObject, 5f);
        Camera.main.GetComponent<Animator>().Play("camera_shake",0,0f);        
        Anim.gameObject.SetActive(false);
        GetComponent<Collider2D>().enabled = false;
        DieAnim.Play("boss_die");
        SoundManager.Post(Collection.BossExplode, gameObject);

        yield return new WaitForSeconds(5f);        
        SoundManager.Post(Collection.ShiftTopEndGame, GameManager.Player.gameObject);
        yield return new WaitForSeconds(2f);
        Lava_Rainbow.TurnIntoRainbow();
        if (GameManager.Player.Alived) {            
            Player.Controllable = false;
            EndGameAnim.Play("endgame_pop_win", 0, 0f);
            SoundManager.Post(Collection.StopElementals, MyCursor.instance.gameObject);
        }
    }

    IEnumerator FrozenCoundown() {
        yield return new WaitForSeconds(3f);
        SoundManager.Post(Collection.BossIceCrack, gameObject);
        yield return new WaitForSeconds(5f);
        Destroy(Instantiate(Resources.Load("IceCrack"), transform.position, Quaternion.identity) as GameObject, 3f);
        Weak = Elemental.Property.Ice;        
    }

    void Start () {
        rb = GetComponent<Rigidbody2D>();        
        BossSprite = Anim.GetComponent<SpriteRenderer>();
	}

    public void Activate() {
        StartCoroutine(Activating());
    }

    IEnumerator Activating() {
        SoundManager.Post(Collection.BossAwake, gameObject);        
        yield return new WaitForSeconds(1f);                
        RootPS.gameObject.SetActive(true);
        SoundManager.Post(Collection.BossBurning, gameObject);
        Anim.Play("boss_awake");
        yield return new WaitForSeconds(5f);
        Activated = true;
    }
	
	// Update is called once per frame
	void Update () {
        Anim.SetBool("Activated", Activated);
        if (!GameManager.Player.Alived) {
            MoveVector = Vector2.zero;
            AttackVector = Vector2.zero;
            InAttackState = false;
        }
        if (InAttackState)
            rb.bodyType = RigidbodyType2D.Static;
        else
            rb.bodyType = RigidbodyType2D.Dynamic;
        AttackUpdate();
        MoveUpdate();        
        if (AttackVector != Vector2.zero) {            
            if (!InAttackState) {                
                Anim.transform.localScale = AttackVector.x > 0 ? new Vector2(1f, 1f) : new Vector2(-1f, 1f);
            }
        }
        else
            Anim.transform.localScale = MoveVector.x > 0 ? new Vector2(1f, 1f) : new Vector2(-1f, 1f);

        float scale = 3f;

        if (Weak == Elemental.Property.Water) {//State 1
            BossSprite.color = Color.Lerp(BossSprite.color, BossStatesColors[0], Time.deltaTime * scale);
            RootPS.startColor = Color.Lerp(RootPS.startColor, ParticleStatesColors[0], Time.deltaTime * scale);
            Color SubColor = ParticleStatesColors[0];
            SubColor.a = 0.38f;
            SubPS.startColor = Color.Lerp(SubPS.startColor, SubColor, Time.deltaTime * scale);
        }
        else if (Weak == Elemental.Property.Ice) {//State 2
            Anim.speed = Mathf.Lerp(Anim.speed, 1f, Time.deltaTime * 1.5f);
            BossSprite.color = Color.Lerp(BossSprite.color, BossStatesColors[1], Time.deltaTime * scale);
            RootPS.startColor = Color.Lerp(RootPS.startColor, ParticleStatesColors[1], Time.deltaTime * scale);
            Color SubColor = ParticleStatesColors[1];
            SubColor.a = 0.38f;
            SubPS.startColor = Color.Lerp(SubPS.startColor, SubColor, Time.deltaTime * scale);
            RootPS.emissionRate = Mathf.Lerp(RootPS.emissionRate, 0f, Time.deltaTime * scale);
            SubPS.emissionRate = Mathf.Lerp(RootPS.emissionRate, 0f, Time.deltaTime * scale);
            Speed = Mathf.Lerp(Speed, 2500f, Time.deltaTime * 1.5f);
        }
        else if (Weak == Elemental.Property.Electric) {//State 3            
            Anim.speed = Mathf.Lerp(Anim.speed, 0, Time.deltaTime*1.5f);
            BossSprite.color = Color.Lerp(BossSprite.color, BossStatesColors[2], Time.deltaTime * scale);
            RootPS.startColor = Color.Lerp(RootPS.startColor, ParticleStatesColors[2], Time.deltaTime * scale);
            Color SubColor = ParticleStatesColors[2];
            SubColor.a = 0.38f;
            SubPS.startColor = Color.Lerp(SubPS.startColor, SubColor, Time.deltaTime * scale);
            Speed = Mathf.Lerp(Speed, 0f, Time.deltaTime * 1.5f);
        }
    }

    private void MoveUpdate() {
        if (!Target.Alived || !Alive || !Activated)
            return;
        float dist = Vector2.Distance(Target.Position, Positon);
        if (dist > AttackDistance && !InAttackState) {
            Vector2 Direction = Vector3.Normalize(Target.Position - Positon);            
            MoveVector = new Vector2(Direction.x > 0f ? 1f : -1f, 0);
            AttackVector = Vector2.zero;
        }
        else if (!InAttackState) {
            MoveVector = Vector2.zero;
        }
        rb.velocity = MoveVector * Speed * Time.deltaTime;
        Anim.SetBool("Moving", MoveVector!= Vector2.zero);
    }
    private void AttackUpdate() {
        if (!Target.Alived || !Alive || !Activated)
            return;
        float dist = Vector2.Distance(Target.Position, Positon);
        if (dist <= AttackDistance && !InAttackState) {
            Vector2 Direction = Vector3.Normalize(Target.Position - Positon);            
            AttackVector = new Vector2(Direction.x > 0f ? 1f : -1f, 0);
            MoveVector = Vector2.zero;
        }
        else if(!InAttackState){
            AttackVector = Vector2.zero;
        }        
        Anim.SetBool("Attacking", AttackVector != Vector2.zero);
     }

    public void PlayAttackEnterSFX() {
        SoundManager.Post(Collection.BossAttackEnter, gameObject);
    }

    public void PlayAttackExitSFX() {
        SoundManager.Post(Collection.BossAttackExit, gameObject);
    }

    public void PlayFootStepSFX() {
        SoundManager.Post(Collection.BossWalk, gameObject);
    }

}
