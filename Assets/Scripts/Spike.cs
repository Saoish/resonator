﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.tag = Tag.Deadly;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.tag == Tag.Player) {
            GameObject Explode = Instantiate(Resources.Load("Explode"), collision.gameObject.transform.position, Quaternion.identity) as GameObject;
            Destroy(Explode, 2f);
            SoundManager.Post(Collection.MetalCrash, collision.gameObject);
        }        
    }
}
