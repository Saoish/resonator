﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public abstract class Elemental : MonoBehaviour{    
    public enum Property { Water,Ice,Fire,Electric};        
    public Vector2 Position { get { return transform.position; } }
    //public static Color C_Wind = Color.white;
    public static Color C_Water = new Color(0.365f, 0.48f, 1f);
    public static Color C_Ice = Color.cyan;
    public static Color C_Fire = new Color(1f, 0.4f, 0f);
    public static Color C_Electric = Color.yellow;

    public static Dictionary<Property, Color> Colors = new Dictionary<Property, Color>() {
        //{Property.Wind,C_Wind },
        {Property.Water,C_Water },
        {Property.Ice,C_Ice },
        {Property.Fire,C_Fire },
        {Property.Electric,C_Electric}
    };

    public static int NumOfProperties {get { return System.Enum.GetValues(typeof(Elemental.Property)).Length;}}

    protected virtual void Awake() {
    }

    protected virtual void Start () {            
	}    
		
	protected virtual void Update () {
	}

    //protected virtual void OnMouseEnter() {
    //    Player.Target = this;        
    //}

    //protected virtual void OnMouseExit() {
    //    Player.Target = null;        
    //}    
}