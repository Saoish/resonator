﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Soul : MonoBehaviour {
    public GameObject[] Effects;
    protected bool instantiated = false;
    protected float speed = 10f;
    protected Player player;
    [HideInInspector]
    public Elemental.Property Property;

    public Rigidbody2D rb;
    public ParticleSystem ps;
    public ParticleSystem sub_emit;
    public Light light;

    protected virtual void Awake() {
        gameObject.layer = CollisionLayer.Soul;
    }

    protected virtual void Start() {        
        player = FindObjectOfType<Player>();
        SoundManager.Post(Collection.SoulLoop, gameObject);        
        ps.startColor = Elemental.Colors[Property];
        sub_emit.startColor = Elemental.Colors[Property];
        light.color = Elemental.Colors[Property];

        SoundManager.Post(Recordable.PropertySFXs[Player.Selected.Property], gameObject);
        Effects[(int)Property].SetActive(true);
    }


    protected IEnumerator LightUpAndDown(float inc_percentage) {//An effect the indicate gaining 
        Destroy(gameObject.GetComponent<LightFlicker>());//if it has one
        Effects[(int)Property].GetComponent<ParticleSystem>().enableEmission = false;
        SoundManager.Post(Collection.StopElementals, gameObject);        
        Light light = GetComponent<Light>();
        Light EffectLight = Effects[(int)Property].GetComponentInChildren<Light>(true);
        float peak_range = light.range + light.range * (inc_percentage/100f);
        float peak_intensity = light.intensity + light.intensity * (inc_percentage / 100f);        

        float IncTime = 1f;
        float Counter = 0f;
        while (Counter < IncTime) {
            Counter += Time.deltaTime;
            float timeProressed = Counter / IncTime;
            light.intensity = Mathf.Lerp(light.intensity, peak_intensity, timeProressed);
            light.range = Mathf.Lerp(light.range, peak_range, timeProressed);
            EffectLight.intensity = Mathf.Lerp(EffectLight.intensity, peak_intensity, timeProressed);
            EffectLight.range = Mathf.Lerp(EffectLight.range, peak_range, timeProressed);
            yield return new WaitForEndOfFrame();
        }

        float FadeTime = 2f;
        Counter = 0f;
        while (Counter < FadeTime) {
            Counter += Time.deltaTime;
            float timeProressed = Counter / FadeTime;
            light.intensity = Mathf.Lerp(light.intensity, 0f, timeProressed);
            light.range = Mathf.Lerp(light.range, 0f, timeProressed);
            EffectLight.intensity = Mathf.Lerp(EffectLight.intensity, 0f, timeProressed);
            EffectLight.range = Mathf.Lerp(EffectLight.range, 0f, timeProressed);
            yield return new WaitForEndOfFrame();
        }
    }

}
