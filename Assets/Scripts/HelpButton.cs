﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class HelpButton : MyButton {
    public CanvasGroup CG;
    bool Helping = false;
        
    private void Awake() {        
    }
    public override void OnPointerClick(PointerEventData eventData) {
        return;
    }

    public override void OnPointerEnter(PointerEventData eventData) {
        base.OnPointerEnter(eventData);
        Helping = true;
    }

    public override void OnPointerExit(PointerEventData eventData) {
        base.OnPointerExit(eventData);
        Helping = false;
    }

    private void Update() {
        CG.alpha = Mathf.Lerp(CG.alpha, Helping? 1f:0f, Time.deltaTime * 6f);
    }
}
