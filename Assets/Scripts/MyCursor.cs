﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCursor : MonoBehaviour {
    bool InEditor = Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.OSXEditor;
    ParticleSystem particle;
    private Light light;

    public static MyCursor instance;

    public static Vector2 Position { get { return instance.transform.position; } }      
    
    private GameObject CachedEffect;
    public GameObject[] Effects;

    private void Awake() {
        if(instance != null && instance != this) {
            Destroy(this.gameObject);
        } else {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }    

    private void Start() {
        particle = GetComponent<ParticleSystem>();
        light = GetComponent<Light>();
    }

    public static void UpdateProperty() {
        SoundManager.Post(Collection.StopElementals, instance.gameObject);
        SoundManager.Post(Recordable.PropertySFXs[Player.Selected.Property], instance.gameObject);
        if (instance.CachedEffect) {
            instance.CachedEffect.SetActive(false);
        }
        instance.CachedEffect = instance.Effects[(int)Player.Selected.Property];
        instance.CachedEffect.SetActive(true);
    }

    private void Update() {
        if (!GameManager.Player.Alived) {
            Cursor.visible = true;
            return;
        }
        if (Application.platform != RuntimePlatform.WindowsEditor || Application.platform != RuntimePlatform.OSXEditor)
            Cursor.visible = false;
        var mousePos = Input.mousePosition;
        mousePos.z = 9.9f;
        this.transform.position = Camera.main.ScreenToWorldPoint(mousePos);        
        //UpdateOrbColor();

        int layer = (1 << CollisionLayer.Recordable | 1<< CollisionLayer.Interactable);
        Collider2D hit = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint(mousePos),0.5f,layer);
        Player.Target = hit != null ? hit.GetComponent<Elemental>() : null;        
    }

    private void UpdateOrbColor() {        
        particle.startColor = Color.Lerp(particle.startColor, Elemental.Colors[Player.Selected.Property], Time.deltaTime * 12f);
        light.color = Color.Lerp(light.color, Elemental.Colors[Player.Selected.Property], Time.deltaTime * 12f);
    }
}
