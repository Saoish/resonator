﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackState : StateMachineBehaviour {

    float AttackEnterPlayThreshold = 0.1f; bool AttackEnteredPlayed = false;

    float AttackExitPlayThrshold = 0.4f; bool AttackExitPlayed = false;

    bool ShockPlayed = false;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Boss boss = animator.transform.GetComponentInParent<Boss>();
        float NormalizedTS = Mathf.Repeat(stateInfo.normalizedTime, 1f);
        if(NormalizedTS <0.99f) {
            boss.InAttackState = true;
        }
        else {
            boss.InAttackState = false;
            boss.MeleeAttackCollider.Hit = false;
        }
        
        if(NormalizedTS<=AttackEnterPlayThreshold && !AttackEnteredPlayed) {
            boss.PlayAttackEnterSFX();
            AttackEnteredPlayed = true;
            AttackExitPlayed = false;
        }else if(NormalizedTS>= AttackExitPlayThrshold && !AttackExitPlayed) {
            boss.PlayAttackExitSFX();
            if (boss.MeleeAttackCollider.Hit) {                
                SoundManager.Post(Collection.PlayerHurt, GameManager.Player.gameObject);               
            }            
            AttackEnteredPlayed = false;
            AttackExitPlayed = true;
        }
        if (NormalizedTS >= 0.5f && !ShockPlayed) {
            ShockPlayed = true;
            Camera.main.GetComponent<Animator>().Play("little_shake", 0, 0f);
        }        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        ShockPlayed = false; 
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
