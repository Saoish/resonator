﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWalkState : StateMachineBehaviour {
    float SecondFootStepThreshold = 0.4f; bool SecondFootStepPlayed = false;
    float ThridFootStepThreshold = 0.9f; bool ThridFootStepPlayed = false;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {        
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        float NormalizedTS = Mathf.Repeat(stateInfo.normalizedTime, 1f);

        if (NormalizedTS >= SecondFootStepThreshold && NormalizedTS < ThridFootStepThreshold && !SecondFootStepPlayed) {
            GameManager.Boss.PlayFootStepSFX();
            SecondFootStepPlayed = true;
            ThridFootStepPlayed = false;
        }
        else if (NormalizedTS >= ThridFootStepThreshold && !ThridFootStepPlayed) {
            GameManager.Boss.PlayFootStepSFX();
            SecondFootStepPlayed = false;
            ThridFootStepPlayed = true;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        SecondFootStepPlayed = ThridFootStepPlayed = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
