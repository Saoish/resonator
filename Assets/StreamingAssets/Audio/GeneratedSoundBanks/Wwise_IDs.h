/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BATSFLYINGOVER = 2203376118U;
        static const AkUniqueID BLIZZARD = 3610151219U;
        static const AkUniqueID BOSSATTACKENTER = 1509554316U;
        static const AkUniqueID BOSSATTACKEXIT = 1344357710U;
        static const AkUniqueID BOSSAWAKE = 306254719U;
        static const AkUniqueID BOSSBURNING = 2260888259U;
        static const AkUniqueID BOSSEXPLODE = 1846098601U;
        static const AkUniqueID BOSSFROZEN = 2794717868U;
        static const AkUniqueID BOSSICECRACK = 1442755795U;
        static const AkUniqueID BOSSWALK = 2129933077U;
        static const AkUniqueID BURNOFFWOODENPLATFORM = 1740214514U;
        static const AkUniqueID BUTTONHOVER = 3035572085U;
        static const AkUniqueID BUTTONSELECT = 2903558085U;
        static const AkUniqueID BUTTONSUBMIT = 2201030189U;
        static const AkUniqueID CHECKPOINTACTIVATION = 562403143U;
        static const AkUniqueID CHECKPOINTLOOP = 2863229777U;
        static const AkUniqueID DECREASEBUSPITCH = 999640593U;
        static const AkUniqueID DIE = 445985469U;
        static const AkUniqueID ELECTRICARCING = 215531308U;
        static const AkUniqueID ELECTRICLOOP = 332551302U;
        static const AkUniqueID ERROR = 1880695829U;
        static const AkUniqueID FLAME = 1590552294U;
        static const AkUniqueID FOOTSTEP = 1866025847U;
        static const AkUniqueID FOOTSTEPCONCRETE = 168554618U;
        static const AkUniqueID FOOTSTEPICE = 825805392U;
        static const AkUniqueID FOOTSTEPLEAF = 2532234983U;
        static const AkUniqueID FOOTSTEPMETAL = 3399837092U;
        static const AkUniqueID FOOTSTEPRUBBER = 81442441U;
        static const AkUniqueID FROZEN = 1763750795U;
        static const AkUniqueID GROWTHPLANT = 4141104487U;
        static const AkUniqueID HARDRESET = 1219561983U;
        static const AkUniqueID HELICOPTERSTART = 1924425818U;
        static const AkUniqueID INCREASELOWPASS = 2272188166U;
        static const AkUniqueID LAVALOOP = 3700128293U;
        static const AkUniqueID LAVASPLASHENTER = 3223573076U;
        static const AkUniqueID LAVASPLASHEXIT = 3582450358U;
        static const AkUniqueID LIGHTINGSTRIKE = 3056934751U;
        static const AkUniqueID METALCRASH = 1478779199U;
        static const AkUniqueID PLAYERHURT = 3537581393U;
        static const AkUniqueID PLAYERLAVAENTER = 3281476826U;
        static const AkUniqueID PLAYERWATERENTER = 3639388275U;
        static const AkUniqueID POTKICKING = 889568968U;
        static const AkUniqueID RESETBUSPITCH = 4035662626U;
        static const AkUniqueID RESETMAINBUSLOWPASS = 192482606U;
        static const AkUniqueID ROBOTMOVE = 1677261228U;
        static const AkUniqueID RUBBERDUCKLAVAENTER = 1726862744U;
        static const AkUniqueID SHIFTTOPENDGAME = 4126488329U;
        static const AkUniqueID SOULEXPLOSION = 3188500461U;
        static const AkUniqueID SOULLOOP = 364988522U;
        static const AkUniqueID SOULSUMMON = 3450266969U;
        static const AkUniqueID STARTBOSSBGM = 1774837546U;
        static const AkUniqueID STARTRECORD = 4202142260U;
        static const AkUniqueID STOPELEMENTALS = 2880223727U;
        static const AkUniqueID STOPLAVALOOP = 19906685U;
        static const AkUniqueID STOPRECORD = 3121475866U;
        static const AkUniqueID STOPSOULLOOP = 967547826U;
        static const AkUniqueID STREAM = 4075078981U;
        static const AkUniqueID SUMMONFAILURE = 3004797750U;
        static const AkUniqueID UNDERWATERLOOP = 1321521464U;
        static const AkUniqueID WATERPUTTINGOUTFIRE = 1205405571U;
        static const AkUniqueID WATERSPLASHENTER = 2906369031U;
        static const AkUniqueID WATERSPLASHEXIT = 508861999U;
        static const AkUniqueID WATERSPRAY = 666441205U;
        static const AkUniqueID WIND = 1537061107U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID SPINNINGSPEED = 1207962052U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MAIN = 3161908922U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
        static const AkUniqueID SECONDARY = 1871372953U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
