# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 1.0.1 - 2017-12-10
### Added
- CHANGELOG.md
- README.md

### Changed
- update to support Unity 2017.2.0f3 and Wwise 2017.1.3.6377

### Fixed
- rubber duck not falling off on Unity 2017.2.0f3

## 1.0.0 - 2017-06-17
### Added
- push to my bitbucket